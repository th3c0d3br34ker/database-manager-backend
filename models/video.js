const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const VideoSchema = new Schema({
  series: { type: String, required: true },
  actress: [{ type: String, trim: true }],
  name: { type: String, trim: true },
  publication_date: { type: Date, default: Date.now(), required: true },
  file_name: { type: String, required: true },
  file_path: { type: String, required: true },
  starred: { type: Boolean, default: false },
  tags: [String],
});

module.exports = mongoose.model('Video', VideoSchema);
