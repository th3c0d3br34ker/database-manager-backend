const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const actressSchema = new Schema({
  actress: String,
  favourite: { type: Boolean, default: false },
  videos: [
    {
      type: mongoose.Schema.ObjectId,
      ref: 'Video',
    },
  ],
});

module.exports = mongoose.model('Actress', actressSchema);
