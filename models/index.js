const VideoSchema = require('./video');
const SettingsSchema = require('./settings');
const ActressSchema = require('./actress');
const ProductionHouseSchema = require('./production-house');

module.exports = {
  VideoSchema,
  ProductionHouseSchema,
  SettingsSchema,
  ActressSchema,
};
