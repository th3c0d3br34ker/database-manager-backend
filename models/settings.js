const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const settingsSchema = new Schema({
  env: { type: String, required: true },
  apiUri: { type: String, required: true },
  videoUri: { type: String, required: true },
  tags: [String],
});

module.exports = mongoose.model('Settings', settingsSchema);
