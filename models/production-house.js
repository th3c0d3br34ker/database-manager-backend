const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProductionHouseSchema = new Schema({
  name: { type: String, required: true },
  series: [String],
});

module.exports = mongoose.model('ProductionHouse', ProductionHouseSchema);
