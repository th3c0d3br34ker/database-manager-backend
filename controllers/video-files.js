const config = require('../config');
const { VideoSchema } = require('../models');

const options = {
  root: config.video,
  dotfiles: 'deny',
  headers: {
    'x-timestamp': Date.now(),
    'x-sent': true,
  },
};

module.exports = {
  sendVideo: async (req, res) => {
    const { id } = req.params;

    const video = await VideoSchema.findById(id);

    const fileName = video.file_path;

    res.sendFile(fileName, options, (err) => {
      if (err) {
        req.log.error(`Error: ${err}`);
      } else {
        req.log.error('Sent:', fileName);
      }
    });
  },
};
