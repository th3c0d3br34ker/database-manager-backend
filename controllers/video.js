const { VideoSchema } = require('../models');

module.exports = {
  getDatabase: async (req, res) => {
    const { limit = 10, skip = 0 } = req.query;

    const videos = await VideoSchema.find()
      .sort({ series: 1, actress: 1 })
      .skip(parseInt(skip))
      .limit(parseInt(limit));

    const count = await VideoSchema.countDocuments();

    req.log.info(
      `Request Successful! items: ${videos.length} limit: ${limit} skip: ${skip}.`
    );

    res.json({ success: true, data: { count, videos } });
  },

  getDatabaseAll: async (req, res) => {
    const videos = await VideoSchema.find().sort({ series: 1, actress: 1 });

    const count = await VideoSchema.countDocuments();

    req.log.info(`Request Successful! items: ${videos.length}.`);

    res.json({ success: true, data: { count, videos } });
  },

  addData: async (req, res) => {
    const { body } = req;
    const { file_name } = body;

    let video = await VideoSchema.findOne({ file_name });

    if (video) {
      await VideoSchema.findOneAndUpdate(
        file_name,
        { ...body },
        { useFindAndModify: false }
      );

      req.log.info('Video Entry Updated Successfully!');
      return res.json({
        success: true,
        message: 'Video Entry Updated Successfully!',
        video,
      });
    } else {
      video = new VideoSchema(body);

      await video.save((err) => {
        if (err) {
          return res
            .status(401)
            .json({ success: false, message: 'Failed to add data' });
        }
      });
      req.log.info('Video Entry Added Successfully!');
      res.json({
        success: true,
        message: 'Video Entry Added Successfully!',
        video,
      });
    }
  },

  updateData: async (req, res) => {
    const { id } = req.params;
    const { body } = req;
    // const { file_name } = body;

    try {
      const video = await VideoSchema.findById(id);

      if (!video) {
        return res.json({ success: false, message: 'Invalid ID!' });
      }

      const newVideo = { ...video._doc, ...body };
      // console.log(newVideo);

      await VideoSchema.findByIdAndUpdate(
        id,
        {
          $set: {
            series: newVideo.series,
            name: newVideo.name,
            publication_date: newVideo.publication_date,
            file_name: newVideo.file_name,
            starred: newVideo.starred,
            file_path: newVideo.file_path,
          },
          $addToSet: {
            actress: newVideo.actress,
            tags: newVideo.tags,
          },
        },
        { useFindAndModify: false, upsert: true, new: false }
      );

      req.log.info(`Video Entry ${id} Updated Successfully!`);

      return res.json({
        success: true,
        message: 'Entry Updated!',
        video: await VideoSchema.findById(id),
      });
    } catch (error) {
      req.log.error(`Error: ${error.name} ${error.message}`);
      return res
        .status(500)
        .json({ success: false, message: `Server Error  ${error.message}` });
    }
  },

  getById: async (req, res) => {
    const { params } = req;
    const { id } = params;

    const video = await VideoSchema.findById(id);

    if (video) {
      return res
        .status(200)
        .json({ success: true, message: 'Entry Found!', video });
    }

    return res.json({ success: false, message: 'No Entry Found!' });
  },

  getByFileName: async (req, res) => {
    const { file_name } = req.query;

    const video = await VideoSchema.findOne({
      file_name,
    });

    if (!video) {
      return res.json({ success: false, message: 'No Entry Found!' });
    }

    return res.json({
      success: true,
      message: 'Entry Found!',
      data: { count: video.length, video },
    });
  },

  getSeriesAll: async (req, res) => {
    const { series } = req.query;

    try {
      const videos = await VideoSchema.find({
        series,
      });

      if (videos && videos.length !== 0) {
        const count = videos.length;

        req.log.info(`Series Found! Sent ${videos.length} items.`);
        return res.json({
          success: true,
          message: 'Entry Found!',
          data: { count: count, videos },
        });
      }

      req.log.info(`No Series Found! ${series}`);
      return res
        .status(200)
        .json({ success: false, message: 'No Entry Found!' });
    } catch (error) {
      req.log.error(`Error: ${error.name} ${error.message}`);
      return res
        .status(500)
        .json({ success: false, message: 'No Entry Found!' });
    }
  },

  getByProductionHouse: async (req, res) => {
    const { series } = req.query;

    const video = await VideoSchema.find({
      series,
    });

    if (video) {
      req.log.info(`Series Found! Sent ${video.length} items.`);

      return res.json({
        success: true,
        message: 'Series Found!',
        data: { count: video.length, video },
      });
    }
    return res.json({ success: false, message: 'No Entry Found!' });
  },

  getByActress: async (req, res) => {
    const { actress } = req.query;

    try {
      const videos = await VideoSchema.find({
        actress,
      });

      const count = videos.length;

      if (videos) {
        req.log.info(`Actress Found! Sent ${videos.length} items.`);

        return res.json({
          success: true,
          message: 'Entry Found!',
          data: { count: count, videos },
        });
      }
    } catch (error) {
      req.log.error(`Error: ${error.name}`);
      return res
        .status(500)
        .json({ success: false, message: `Server Error! ${error.message}` });
    }
  },
};
