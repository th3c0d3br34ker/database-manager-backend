const { VideoSchema, ActressSchema } = require('../models');

module.exports = {
  getActress: async (req, res) => {
    try {
      const videos = await VideoSchema.find().sort({ series: 1 });

      const actressSet = new Set();

      videos.forEach((element) => {
        const { actress } = element;

        actress.forEach((item) => {
          actressSet.add(item);
        });
      });

      const actresses = [];

      actressSet.forEach((name) => {
        actresses.push({
          actress: name,
        });
      });

      actresses.sort((a, b) => {
        // converting to uppercase to have case-insensitive comparison
        const name1 = a.actress.toUpperCase();
        const name2 = b.actress.toUpperCase();

        let comparison = 0;

        if (name1 > name2) {
          comparison = 1;
        } else if (name1 < name2) {
          comparison = -1;
        }
        return comparison;
      });

      req.log.info(`Request Successful! items: ${actresses.length}.`);

      return res.json({
        success: true,
        data: { count: actresses.length, actresses },
      });
    } catch (error) {
      req.log.error(`Error: ${error.name}`);
      return res
        .status(500)
        .json({ success: false, message: `Server Error! ${error.message}` });
    }
  },

  getActressesNew: async (req, res) => {
    try {
      const actresses = await ActressSchema.find()
        .sort({ actress: 1 })
        .populate('videos');
      const count = await ActressSchema.countDocuments();

      req.log.info(`Request Successful! items: ${count}.`);

      return res.json({ success: true, data: { count, actresses } });
    } catch (error) {
      req.log.error(`Error: ${error.name}`);
      return res
        .status(500)
        .json({ success: false, message: `Server Error! ${error.message}` });
    }
  },

  getActressNew: async (req, res) => {
    try {
      const { actress } = req.query;

      const actressData = await ActressSchema.findOne({
        actress,
      }).populate('videos');

      if (actressData) {
        req.log.info(`Request Successful! Actress: ${actress}`);

        return res.json({
          success: true,
          message: 'Actress found!',
          data: { actress: actressData },
        });
      } else {
        req.log.error('Failed to find actress!');

        return res.json({
          success: false,
          message: 'Actress not found!',
        });
      }
    } catch (error) {
      req.log.error(`Error: ${error.name}`);
      return res
        .status(500)
        .json({ success: false, message: `Server Error! ${error.message}` });
    }
  },

  updateActress: async (req, res) => {
    try {
      const { body } = req;

      const videos = await VideoSchema.find({ actress: body.actress }).sort({
        series: 1,
      });

      const count = await VideoSchema.countDocuments({ actress: body.actress });

      const actressData = await ActressSchema.findOne({
        actress: body.actress,
      });

      if (actressData) {
        await ActressSchema.findOneAndUpdate(
          {
            actress: body.actress,
          },
          {
            ...body,
            videos,
          },
          {
            useFindAndModify: false,
          }
        );

        req.log.info(`Request Successful! Actress: ${body.actress} Updated.`);

        return res.json({
          success: true,
          message: `Updated Successfully! Actress: ${body.actress} Updated.`,
          data: {
            count,
            actress: await ActressSchema.findOne({
              actress: body.actress,
            }).populate('videos'),
          },
        });
      } else {
        const actressData = await new ActressSchema({ ...body, videos });
        await actressData.save((err) => {
          if (err) {
            return res.json({
              success: false,
              message: 'Failed to Update Actress',
            });
          }
          req.log.info(
            `Request Successful! Actress: ${body.actress} Created and Updated.`
          );

          return res.json({
            success: true,
            message: `Updated Successfully! Actress: ${body.actress} Created and Updated.`,
            data: { count, actress: actressData },
          });
        });
      }
    } catch (error) {
      req.log.error(`Error: ${error.name}`);
      return res
        .status(500)
        .json({ success: false, message: `Server Error! ${error.message}` });
    }
  },
};
