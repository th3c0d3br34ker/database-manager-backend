const actress = require('./actress');
const basic = require('./basic');
const productionHouse = require('./production-house');
const video = require('./video');
const videoFiles = require('./video-files');
const settings = require('./settings');

module.exports = {
  actress,
  basic,
  video,
  videoFiles,
  productionHouse,
  settings,
};
