const { SettingsSchema } = require('../models');

module.exports = {
  getSettings: async (req, res) => {
    const { env } = req.query;

    try {
      const settings = await SettingsSchema.findOne({ env }, { _id: 0 }).sort({
        tags: 1,
      });

      if (settings) {
        req.log.info('Settings Sent Successfully!');
        return res.json({
          success: true,
          message: 'Settings Found!',
          data: settings,
        });
      }

      req.log.error('Error in gettings settings!');
      res
        .status(200)
        .json({ success: false, message: 'Error settings not found.' });
    } catch (error) {
      req.log.error(`Error: ${error}`);
      return res.status(500).json({ success: false, message: 'Server Error!' });
    }
  },

  updateSettings: async (req, res) => {
    const { body } = req;
    const { env } = body;
    try {
      const settings = await SettingsSchema.findOne({ env });

      if (settings) {
        let { apiUri, videoUri, tags } = body;

        apiUri = apiUri || settings.apiUri;
        videoUri = videoUri || settings.videoUri;
        tags = (tags && [...tags, ...settings.tags].sort()) || settings.tags;

        await SettingsSchema.findOneAndUpdate(
          {
            env,
          },
          {
            apiUri,
            videoUri,
            tags,
          },
          { useFindAndModify: false, upsert: true }
        );

        req.log.info('Request Successfull! Settings Updated!');

        return res.json({
          success: true,
          message: 'Settings Updated!',
          data: await SettingsSchema.findOne({ env }),
        });
      } else {
        const settings = new SettingsSchema(body);

        await settings.save((err) => {
          if (err) {
            req.log.error(
              `Error in saving settings! ${err.name} ${err.message}`
            );

            return res.json({
              success: false,
              message: 'Error in saving settings!',
            });
          }

          req.log.info('Settings Created!');

          return res.json({
            success: true,
            message: 'Settings Created!',
            settings,
          });
        });
      }
    } catch (error) {
      req.log.error(`Error: ${error.name} ${error.message}`);

      return res
        .status(500)
        .json({ success: false, message: `Server Error! ${error.message}` });
    }
  },
};
