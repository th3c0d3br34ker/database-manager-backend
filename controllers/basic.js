module.exports = {
  home: (req, res) => {
    req.log.info('API Check! Status OK');
    return res
      .status(200)
      .json({ success: true, message: 'API Check!', data: 'OK' });
  },
};
