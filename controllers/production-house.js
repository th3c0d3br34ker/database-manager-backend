const { ProductionHouseSchema } = require('../models');

module.exports = {
  getProdutionHouses: async (req, res) => {
    const productionHouses = await ProductionHouseSchema.find().sort({
      name: 1,
    });

    const count = await ProductionHouseSchema.countDocuments();

    res.json({
      success: true,
      data: { count, productionHouses },
    });
  },

  addProductionHouse: async (req, res) => {
    const { body } = req;

    const { name, series } = body;

    try {
      const productionHouse = await ProductionHouseSchema.findOne({
        name: name,
      });

      if (productionHouse) {
        await ProductionHouseSchema.findOneAndUpdate(
          {
            name: name,
          },
          {
            $addToSet: {
              series,
            },
          },
          {
            useFindAndModify: false,
            upsert: true,
          }
        );

        req.log.info('Production House Entry Updated Successfully!', name);

        return res.status(201).json({
          success: true,
          message: 'Production House Entry Updated Successfully!',
          data: await ProductionHouseSchema.findOne({
            name,
          }).sort({ series: 1 }),
        });
      }

      const newProductionHouse = new ProductionHouseSchema(body);

      await newProductionHouse.save((err) => {
        if (err) {
          return res
            .status(401)
            .json({ success: false, message: 'Failed to add data!' });
        }
      });

      req.log.info('Production House Entry Added Successfully!');

      return res.status(201).json({
        success: true,
        message: 'Production House Entry Added Successfully!',
        data: newProductionHouse,
      });
    } catch (error) {
      req.log.error(`Error: ${error.name} ${error.message}`);
      return res
        .status(401)
        .json({ success: false, message: 'Failed to add data!' });
    }
  },
};
