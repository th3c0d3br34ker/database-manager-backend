require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const routes = require('./config/routes');
const logger = require('./config/logger.js');

const app = express();

logger.init(app);

const PORT = process.env.PORT || 5050;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static(config.root));
app.use(express.static(config.video));

app.use('/', routes);
app.use('/view-video', express.static(config.videoRoot));

const connect = () => {
  const options = {
    keepAlive: 1,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  };
  mongoose.connect(config.DATABASE, options);
  return mongoose.connection;
};

const listen = () => {
  app.listen(PORT, () => {
    console.log(`Success! Server is running on port ${PORT}.`);
  });
};

const connection = connect();

connection
  .on('error', console.error.bind(console, 'MongoDB connection error:'))
  .on('disconnected', connect)
  .once('open', listen);
