const pino = require('pino-http')({
  base: null,
  prettyPrint: {
    crlf: true,
    translateTime: true,
    messageFormat: '{levelLabel} - {req.method} {req.url} - {msg}',
    hideObject: true, // --hideObject
    singleLine: false, // --singleLine
  },
  autoLogging: false,
});

const logger = {};

const init = (app) => {
  app.use(pino);
};

logger.init = init;

module.exports = logger;
