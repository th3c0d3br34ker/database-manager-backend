const express = require('express');
const { actress } = require('../../controllers');
const router = express.Router();

router.get('/get-all-actresses', actress.getActress);
router.get('/get-actress', actress.getActressNew);
router.get('/get-actresses', actress.getActressesNew);
router.post('/add-actress', actress.updateActress);

module.exports = router;
