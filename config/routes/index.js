const express = require('express');

const router = express.Router();

const actressRotes = require('./actresses-routes');
const apiRoutes = require('./api-routes');
const videoRoutes = require('./video-routes');
const settingsRoutes = require('./settings-routes');

router.use('/api', apiRoutes);
router.use('/api/actress', actressRotes);
router.use('/api/settings', settingsRoutes);
router.use('/video', videoRoutes);

module.exports = router;
