const express = require('express');
const { videoFiles } = require('../../controllers');

const router = express.Router();

router.get('/:id', videoFiles.sendVideo);

module.exports = router;
