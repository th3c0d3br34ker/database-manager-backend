const express = require('express');
const { settings } = require('../../controllers');
const router = express.Router();

router.get('/', settings.getSettings);
router.post('/', settings.updateSettings);

module.exports = router;
