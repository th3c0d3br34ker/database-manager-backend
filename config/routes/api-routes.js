const express = require('express');
const { basic, video, productionHouse } = require('../../controllers');

const router = express.Router();

router.get('/', basic.home);
router.get('/get-data', video.getDatabase);
router.get('/get-data/all', video.getDatabaseAll);
router.post('/add-data', video.addData);
router.post('/update-data/:id', video.updateData);
router.get('/get-by-id/:id', video.getById);
router.get('/get-all-by-series', video.getSeriesAll);
router.get('/get-by-file-name', video.getByFileName);
router.get('/get-by-actress', video.getByActress);

router.get('/get-production-houses', productionHouse.getProdutionHouses);
router.post('/add-production-house', productionHouse.addProductionHouse);

module.exports = router;
