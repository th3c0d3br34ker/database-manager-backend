const path = require('path');
const development = require('./env/development');

const defaults = {
  root: path.normalize(path.join(__dirname, '/..')),
  video: path.normalize('G:/Collection'),
  videoRoot: path.normalize(path.join(__dirname, '/../../../Collection')),
};

module.exports = {
  development: Object.assign({}, development, defaults),
}[process.env.NODE_ENV || 'development'];
